**Disclaimer:** <br>
I'm not a coder. This probably won't work for your site out of the
box. This was made for Python3, but the sytax might work for Python2 - idk.
The code could be cleaner, I'm sure. Submit a pull request and we can fix it
together. <3

**Requirements:**
* Python3
* Selenium
* Pip (optional)
* Chrome or Firefox drivers

<br>

**Installation:**
1.  Install Python3
    I used 3.6.9
1.  Install pip for Python3
    sudo apt install python3-pip
1.  Install Selenium
    sudo pip3 install -U selenium
1.  Install your preferred browser driver
    https://selenium-python.readthedocs.io/installation.html#drivers
    I only tested with Chrome.
1.  Go through the code and update the url, user, pass, and domain (lines 26,27,
    28,29) <br>
    The lines will inevitably change and I'll inevitably forget to update this.
1.  Login manually, click the Home icon and under "My Menu" click "edit MyMenu"
    and move "Timesheet" to the left column

***Note:** <br>
idk know what distro or OS you're using, so these instructions may not
be exact. 

**Usage:** <br>
`python3 ts <hours>` <br>
Ex. `$python3 12` <br>
This will determine the day, launch your browser, and add 12 hours to todat's
date and save. <br>
If an hour isn't specified, it will default to 8. <br>

