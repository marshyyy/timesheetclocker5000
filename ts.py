#!/usr/bin/python3

from datetime import date
import time
import calendar
import os
import selenium
from selenium import webdriver
from selenium.webdriver.common.by import By
import sys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


if len(sys.argv) > 1:
    hours = sys.argv[1]
else:
    hours = 8
    
get_today = date.today()
today = get_today.strftime('%A')
driver = webdriver.Chrome()
wait = WebDriverWait(driver,10)

# Update these #
url = ''
user = ''
password = ''
domain = ''



driver.get(url) 

time.sleep(2)

user_name = driver.find_element_by_id('uid')
user_name.send_keys(user) 

pass_field = driver.find_element_by_id('passField')
pass_field.send_keys(password)

dom_field = driver.find_element_by_id('dom')
dom_field.send_keys(domain)

login_button = driver.find_element_by_id('loginButton')
login_button.click()

time.sleep(1)

driver.switch_to.frame('unitFrame')
driver.find_element_by_xpath("//*[@id='menu_valueSpan']/a").click()

time.sleep(5)

if today == 'Monday':
    driver.find_element_by_id("hrs0_2").click()
    editor = driver.find_element_by_id("editor")
    editor.send_keys('hours')
    driver.find_element_by_id("hrs0_2").click()
    driver.find_element_by_id('appOptionsDivsaveTS').click()
    time.sleep(5)
    driver.quit()
elif today == 'Tuesday':
    driver.find_element_by_id("hrs0_3").click()
    editor = driver.find_element_by_id("editor")
    editor.send_keys('hours')
    driver.find_element_by_id("hrs0_3").click()
    driver.find_element_by_id('appOptionsDivsaveTS').click()
    time.sleep(5)
    driver.quit()
elif today == 'Wednesday':
    driver.find_element_by_id("hrs0_4").click()
    editor = driver.find_element_by_id("editor")
    editor.send_keys('hours')
    driver.find_element_by_id("hrs0_4").click()
    driver.find_element_by_id('appOptionsDivsaveTS').click()
    time.sleep(5)
    driver.quit()
elif today == 'Thursday':
    wait.until(EC.element_to_be_clickable((By.ID, "hrs0_5")))
    driver.find_element_by_id("hrs0_5").click()
    editor = driver.find_element_by_id("editor")
    editor.send_keys(hours)
    driver.find_element_by_id("hrs0_5").click()
    driver.find_element_by_id('appOptionsDivsaveTS').click()
    time.sleep(5)
    driver.quit()
elif today == 'Friday':
    driver.find_element_by_id("hrs0_6").click()
    editor = driver.find_element_by_id("editor")
    editor.send_keys('hours')
    driver.find_element_by_id("hrs0_6").click()
    driver.find_element_by_id('appOptionsDivsaveTS').click()
    time.sleep(5)
    driver.quit()
elif today == 'Saturday':
    driver.find_element_by_id("hrs0_0").click()
    editor = driver.find_element_by_id("editor")
    editor.send_keys('hours')
    driver.find_element_by_id("hrs0_0").click()
    driver.find_element_by_id('appOptionsDivsaveTS').click()
    time.sleep(5)
    driver.quit()
elif today == 'Sunday':
    driver.find_element_by_id("hrs0_1").click()
    editor = driver.find_element_by_id("editor")
    editor.send_keys('hours')
    driver.find_element_by_id("hrs0_1").click()
    driver.find_element_by_id('appOptionsDivsaveTS').click()
    time.sleep(5)
    driver.quit()